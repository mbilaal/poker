<?php

namespace PokerHand;

class PokerHand
{

    protected $hands;

    public function __construct($hand)
    {
        $this->hands = $hand;
    }

    public function getRank()
    {

        $pieces = explode(" ", $this->hands);

        //Splits into pieces
        $p1 =  $pieces[0]; // piece1
        $p2 =  $pieces[1]; // piece2
        $p3 =  $pieces[2]; // piece3
        $p4 =  $pieces[3]; // piece4
        $p5 =  $pieces[4]; // piece5

        //Getting Cards Suits
        $cardOneSuit    = substr($p1, -1); // Card One Suit
        $cardTwoSuit    = substr($p2, -1); // Card Two Suit
        $cardThreeSuit  = substr($p3, -1); // Card Three Suit
        $cardFourSuit   = substr($p4, -1); // Card Four Suit
        $cardFiveSuit   = substr($p5, -1); // Card Five Suit

        //Getting Cards amount
        $p1CardAmount = substr($p1, 0,-1); //Card One Amount
        $p2CardAmount = substr($p2, 0,-1); //Card Two Amount
        $p3CardAmount = substr($p3, 0,-1); //Card Three Amount
        $p4CardAmount = substr($p4, 0,-1); //Card Four Amount
        $p5CardAmount = substr($p5, 0,-1); //Card Five Amount


        //For Card One Value
        if($p1CardAmount == 'J') { $cardOne = '11'; }
        else if($p1CardAmount == 'Q') { $cardOne = '12'; }
        else if($p1CardAmount == 'K') { $cardOne = '13'; }
        else if($p1CardAmount == 'A') { $cardOne = '14'; }
        else { $cardOne = $p1CardAmount; }

        //For Card Two Value
        if($p2CardAmount == 'J') { $cardTwo = '11'; }
        else if($p2CardAmount == 'Q') { $cardTwo = '12'; }
        else if($p2CardAmount == 'K') { $cardTwo = '13'; }
        else if($p2CardAmount == 'A') { $cardTwo = '14'; }
        else { $cardTwo = $p2CardAmount; }

        //For Card Three Value
        if($p3CardAmount == 'J') { $cardThree = '11'; }
        else if($p3CardAmount == 'Q') { $cardThree = '12'; }
        else if($p3CardAmount == 'K') { $cardThree = '13'; }
        else if($p3CardAmount == 'A') { $cardThree = '14'; }
        else { $cardThree = $p3CardAmount; }

        //For Card Four Value
        if($p4CardAmount == 'J') { $cardFour = '11'; }
        else if($p4CardAmount == 'Q') { $cardFour = '12'; }
        else if($p4CardAmount == 'K') { $cardFour = '13'; }
        else if($p4CardAmount == 'A') { $cardFour = '14'; }
        else { $cardFour = $p4CardAmount; }

        //For Card Five Value
        if($p5CardAmount == 'J') { $cardFive = '11'; }
        else if($p5CardAmount == 'Q') { $cardFive = '12'; }
        else if($p5CardAmount == 'K') { $cardFive = '13'; }
        else if($p5CardAmount == 'A') { $cardFive = '14'; }
        else { $cardFive = $p5CardAmount; }


        //CALCULATIONS...
        $totalCardAmount = $cardOne + $cardTwo + $cardThree + $cardFour + $cardFive;


        $cardsArray = array();
        array_push($cardsArray, $cardOne, $cardTwo, $cardThree, $cardFour, $cardFive);
        $cardSuitsArray = array();
        array_push($cardSuitsArray, $cardOneSuit, $cardTwoSuit, $cardThreeSuit,$cardFourSuit, $cardFiveSuit);


        foreach($cardsArray as $card)
        {
            $countCards[(string)$card]++;
        }

        foreach($cardSuitsArray as $suit)
        {
            $countSuits[$suit]++;
        }




        // ROYAL FLUSH [Ten, Jack, Queen, King, Ace all in the same suit.]
        if(($totalCardAmount == "60") && ($countSuits[$cardOneSuit] == 5))
        {
            return "Royal Flush";
        }

        // One Pair [One pair of the same card with three other non-matching cards.]
        else if (count(array_keys($countCards, 2)) === 1)
        {
            return "One Pair";
        }

        // TWO PAIR [Two different pairings or sets of the same card in one hand.]
        else if (count(array_keys($countCards, 2)) === 2)
        {
            return "Two Pair";
        }

        // FLUSH [Five cards, all in one suit, but not in numerical order.]
        else if ($countSuits[$cardOneSuit] == 5 || $countSuits[$cardTwoSuit] == 5 || $countSuits[$cardThreeSuit] == 5)
        {
            return "Flush";
        }

        // TODO: Implement poker hand ranking


    }
}